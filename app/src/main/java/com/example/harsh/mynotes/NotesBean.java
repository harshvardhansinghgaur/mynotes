package com.example.harsh.mynotes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by harsh on 3/11/17.
 */

public class NotesBean implements Parcelable {
    int _id;
    String title;
    String description;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this._id);
        parcel.writeString(this.title);
        parcel.writeString(this.description);
    }

    public NotesBean(Parcel parcel) {
        this._id = parcel.readInt();
        this.title = parcel.readString();
        this.description = parcel.readString();
    }

    public NotesBean() {
    }

    public static final Parcelable.Creator<NotesBean> CREATOR = new Parcelable.Creator<NotesBean>() {
        @Override
        public NotesBean createFromParcel(Parcel source) {
            return new NotesBean(source);
        }

        @Override
        public NotesBean[] newArray(int size) {
            return new NotesBean[size];
        }
    };
}
