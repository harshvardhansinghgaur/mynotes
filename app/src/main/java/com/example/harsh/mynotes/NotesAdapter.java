package com.example.harsh.mynotes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by harsh on 3/11/17.
 */

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {
    private List<NotesBean> dataList;
    AppCompatActivity ctx;

    public NotesAdapter(AppCompatActivity ctx, List<NotesBean> dataList) {
        this.dataList = dataList;
        this.ctx = ctx;
    }

    @Override
    public NotesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notes_recycler_item, parent, false);
        return new NotesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NotesAdapter.MyViewHolder holder, int position) {
        final NotesBean note = dataList.get(position);
        holder.label.setText(note.getTitle());
        holder.descTxt.setText(note.getDescription());
        getDataList();
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public List<NotesBean> getDataList() {
        return dataList;
    }
    public void setDataList(List<NotesBean> dataList1) {
        this.dataList=dataList1;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView label, descTxt;


        public MyViewHolder(View view) {
            super(view);
            label = (TextView) view.findViewById(R.id.title);
            descTxt = (TextView) view.findViewById(R.id.description);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ctx, EditNoteActivity.class);
                    intent.putExtra("noteToEdit",dataList.get(getAdapterPosition()));
                    ctx.startActivityForResult(intent,1);
                }
            });
        }

    }


}
