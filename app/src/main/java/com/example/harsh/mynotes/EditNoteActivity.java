package com.example.harsh.mynotes;

import android.content.ContentValues;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditNoteActivity extends BaseActivity {
    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.save)
    Button save;

    ContentValues values;
    NotesBean notesBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
        //init butterknife
        ButterKnife.bind(this);
        //getExtraData

        values = new ContentValues();
        if (getIntent().getExtras() != null) {
            notesBean = (NotesBean) getIntent().getExtras().getParcelable("noteToEdit");
            title.setText(notesBean.getTitle());
            description.setText(notesBean.getDescription());

        }


    }

    /*
    * called when save button clicked
    * */
    @OnClick(R.id.save)
    void saveBtnClicked() {

        values.put(NotesContentProvider.TITLE,
                title.getText().toString());

        values.put(NotesContentProvider.DESCRIPTION,
                description.getText().toString());
        if (notesBean != null) {
            //update note

            getContentResolver().update(NotesContentProvider.CONTENT_URI, values,NotesContentProvider._ID,new String[] {String.valueOf(notesBean.get_id())});

        }else {
            // Add a new note


            Uri uri = getContentResolver().insert(
                    NotesContentProvider.CONTENT_URI, values);

            Toast.makeText(getBaseContext(),
                    uri.toString(), Toast.LENGTH_LONG).show();
        }
        setResult(RESULT_OK);
        finish();
    }
}
