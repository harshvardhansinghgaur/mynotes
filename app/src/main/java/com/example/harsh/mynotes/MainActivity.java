package com.example.harsh.mynotes;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {
    @BindView(R.id.place_holder)
    TextView placeHolder;
    @BindView(R.id.header_recycler_view)
    RecyclerView header_recycler_view;
    @BindView(R.id.bottom_recycler_view)
    RecyclerView bottom_recycler_view;
    @BindView(R.id.add_note)
    FloatingActionButton add_note;

    List<NotesBean> notesList = new ArrayList<>();
    List<NotesBean> headerList = new ArrayList<>();
    HashMap<String,NotesBean> priorityHasmap=new HashMap<>();
    private Object notes;
    NotesAdapter bottomAdapter, upperAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //init butterknife
        ButterKnife.bind(this);
        //Init
        initBothRecyclerViews();
        //get data from deep link
        if (getIntent().getData() != null && !getIntent().getData().getPath().equals("")) {
            Uri uri = getIntent().getData();
            String title = uri.getQueryParameter("note-title");
            for (int i=0;i<notesList.size();i++){
                if (notesList.get(i).getTitle().equalsIgnoreCase(title)){
                    Intent intent = new Intent(this, EditNoteActivity.class);
                    intent.putExtra("noteToEdit",notesList.get(i));
                    startActivityForResult(intent,1);
                }
            }
        }



    }

    private void initBothRecyclerViews() {
        LinearLayoutManager horizontalLayoutManeger = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        GridLayoutManager gridLayoutManeger = new GridLayoutManager(this, 2);
        // Retrieve notes
        getNotes();
        bottomAdapter = new NotesAdapter(this, notesList);
        upperAdapter = new NotesAdapter(this, headerList);
        header_recycler_view.setLayoutManager(horizontalLayoutManeger);
        bottom_recycler_view.setLayoutManager(gridLayoutManeger);
        header_recycler_view.setAdapter(upperAdapter);
        bottom_recycler_view.setAdapter(bottomAdapter);


    }

    /*
    * add new note
    * */
    @OnClick(R.id.add_note)
    void addNote() {
        Intent intent = new Intent(MainActivity.this, EditNoteActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1) {
            getNotes();
            upperAdapter.setDataList(headerList);
            bottomAdapter.setDataList(notesList);
            upperAdapter.notifyDataSetChanged();
            bottomAdapter.notifyDataSetChanged();
        }
    }

    /*
    * method to retrieve whole data from database
    * */

    public void getNotes() {
        String URL = "content://com.example.MyNotes.NotesContentProvider";

        Uri notes = Uri.parse(URL);
        Cursor c = getContentResolver().query(notes, null, null, null, NotesContentProvider._ID);

        if (c != null && c.moveToFirst()) {
            placeHolder.setVisibility(View.GONE);
            notesList.clear();
            do {
                NotesBean notesBean = new NotesBean();
                notesBean.set_id(c.getInt(c.getColumnIndex(NotesContentProvider._ID)));
                notesBean.setTitle(c.getString(c.getColumnIndex(NotesContentProvider.TITLE)));
                notesBean.setDescription(c.getString(c.getColumnIndex(NotesContentProvider.DESCRIPTION)));
                notesList.add(notesBean);
            } while (c.moveToNext());
                headerList.clear();
                for (int i=0;i<notesList.size();i++){
                    if (headerList.size()==3) break;
                    headerList.add(notesList.get(notesList.size()-(i+1)));
                }


        }

    }

}
